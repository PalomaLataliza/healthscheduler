import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/novaConsulta', title: 'Agendar nova consulta',  icon: 'add', class: '' },
    { path: '/novoPaciente', title: 'Cadastrar paciente',  icon: 'person_add', class: '' },
    { path: '/pacientes', title: 'Listagem de pacientes',  icon: 'assignment_ind', class: '' },
    { path: '/consultas', title: 'Listagem de consultas',  icon: 'assignment', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  
  isMobileMenu() {
      return $(window).width() <= 991;
  };
}
